# HubKids based on template "Synthetica" One Page Website Template

## Credits

*   [Bootstrap](http://getbootstrap.com/)
*   [jQuery](https://jquery.com)
*   [Modernizr](https://modernizr.com/)
*   [Animate.css](https://daneden.github.io/animate.css/)
*   [Stroke Gap Icons](http://graphicburger.com/stroke-gap-icons-webfont/)
*   [Unsplash](http://unsplash.com/)
*   [Startup Stock](http://startupstockphotos.com/)
*   [Pexel](https://www.pexels.com/)
*   [Retina.js](http://imulus.github.io/retinajs/)
*   [Waypoints.js](http://imakewebthings.com/waypoints/)
